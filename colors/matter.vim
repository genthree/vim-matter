" vim-matter: A vim colorscheme
"
" Author: nyronium <nyronium@genthree.io>
" Website: https://gitlab.com/genthree/vim-matter


hi clear

let g:colors_name = "matter"
set background=dark


" ╓───────────────╖
" ║  Main Syntax  ║
" ╙───────────────╜

hi Normal           guifg=#d4d8e0  guibg=#2d3138
hi SpecialComment   guifg=#6e97b7                 gui=bold
hi Debug            guifg=#ddb800                 gui=bold
hi Underlined       guifg=#b3b6bd                 gui=underline

hi Comment          guifg=#8a8d97
hi CommentURL       guifg=#6e97b7                 gui=underline
hi CommentEmail     guifg=#6e97b7                 gui=underline

hi Constant         guifg=#d0a070
hi String           guifg=#cfc87c
hi Character        guifg=#d0a070
hi Number           guifg=#d0a070
hi Boolean          guifg=#a3c288                 gui=bold
hi link Float       Number

hi Identifier       guifg=#67b6b7
hi Function         guifg=#7eaeb7

hi Statement        guifg=#bf686e                 gui=bold
hi Conditional      guifg=#a476b5                 gui=bold
hi link Repeat      Conditional
hi Label            guifg=#cfc87c                 gui=bold
hi Operator         guifg=#bf686e                 gui=bold
hi link Keyword     Statement
hi link Exception   Statement

hi PreProc          guifg=#a476b5                 gui=bold
hi link Include     PreProc
hi link Define      PreProc
hi link Macro       Define
hi PreCondit        guifg=#91b5b5                 gui=bold

hi Type             guifg=#9ec27e                 gui=bold
hi StorageClass     guifg=#6e97b7                 gui=bold
hi link Structure   StorageClass
hi link Typedef     Type

hi Special          guifg=#e7f6da
hi SpecialChar      guifg=#91b5b5
hi SpecialKey       guifg=#91b5b5  guibg=#3d434d

hi Tag              guifg=#e7f6da                 gui=bold
hi link Delimiter   SpecialChar


" ╓────────╖
" ║  Misc  ║
" ╙────────╜

hi Todo             guifg=#f1f41b  guibg=#474e59  gui=bold
hi Error            guifg=#f8f1f1  guibg=#901010  gui=bold,underline
hi ErrorMsg         guifg=#f8f1f1  guibg=#901010  gui=bold
hi WarningMsg       guifg=#edc830


" ╓─────────╖
" ║  Diffs  ║
" ╙─────────╜

hi DiffAdd                         guibg=#335238
hi DiffChange                      guibg=#384e61
hi DiffText         guifg=#2d3138  guibg=#dce077
hi DiffDelete       guifg=#2d3138  guibg=#573437


" ╓──────────────────╖
" ║  Spell checking  ║
" ╙──────────────────╜

hi SpellBad                                       gui=undercurl  guisp=#e25227
hi SpellCap                                       gui=undercurl  guisp=#457afb
hi SpellLocal                                     gui=undercurl  guisp=#48b040
hi SpellRare                                      gui=undercurl  guisp=#e7f6da


" ╓──────────────────╖
" ║  User interface  ║
" ╙──────────────────╜

hi Search           guifg=#08090a  guibg=#f8cf00  gui=bold
hi IncSearch        guifg=#08090a  guibg=#f88d00  gui=bold

hi Pmenu            guifg=#c1c4c6  guibg=#3e4652
hi PmenuSel         guifg=#202224  guibg=#5292c9
hi PmenuSbar                       guibg=#363d47
hi PmenuThumb                      guibg=#515c6b

hi Directory        guifg=#6e97b7                 gui=bold

hi WildMenu         guifg=#202224  guibg=#5292c9
hi Question         guifg=#202224  guibg=#5292c9
hi MoreMsg          guifg=#202224  guibg=#5292c9
hi ModeMsg          guifg=#202224  guibg=#5292c9

hi Cursor           guifg=#e1e6eb  guibg=#7a4d6e
hi CursorLineNr     guifg=#d0d3db  guibg=#26465f  gui=bold
hi CursorLine                      guibg=#353a42
hi CursorColumn                    guibg=#353a42
hi MatchParen       guifg=#202020  guibg=#51c1ea
hi ColorColumn                     guibg=#26465f

hi Visual                          guibg=#3a4d6e
hi VisualNOS                       guibg=#4a4d4e

hi SignColumn       guifg=#51c1ea  guibg=#1f2126
hi FoldColumn       guifg=#757c80  guibg=#1f2126
hi Folded           guifg=#9fa9ad  guibg=#373a3f

hi NonText          guifg=#7a8285  guibg=#25282e
hi Conceal          guifg=#e6e3c8  guibg=#2d3138

hi LineNr           guifg=#7a8285  guibg=#25282e
hi StatusLine       guifg=#a7adba  guibg=#4c5761  gui=NONE
hi StatusLineNC     guifg=#65737e  guibg=#303942  gui=NONE

hi VertSplit        guifg=#414a52  guibg=#303942  gui=NONE
hi Title            guifg=#e6e3c8  guibg=#473242  gui=bold

hi TabLine          guifg=#a7adba  guibg=#4c5761  gui=NONE
hi TabLineSel       guifg=#d0d3db  guibg=#26465f  gui=NONE
hi TabLineFill      guifg=#65737e  guibg=#303942  gui=NONE


" ╓─────╖
" ║  C  ║
" ╙─────╜

hi link cOctalZero  SpecialChar
hi link cDefine     Define
hi link cIncluded   SpecialChar


" ╓───────╖
" ║  C++  ║
" ╙───────╜

hi link cppModifier Label
hi link cppOperator Operator

" e.g. scope in "scope::abc";
let g:cpp_class_scope_highlight = 1
hi link cCustomClass Constant


" ╓─────────╖
" ║  CtrlP  ║
" ╙─────────╜

" https://github.com/kien/ctrlp.vim
hi CtrlPMatch       guifg=#08090a  guibg=#f8cf00
